import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gmxanal", # Replace with your own username
    version="0.0.1",
    author="Gergely Kohut",
    author_email="kohut.gergely@gmail.com",
    description="A helper package for GROMACS+MDAnalysis analyses!",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
