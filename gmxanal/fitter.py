import MDAnalysis
from collections import namedtuple
from gmxanal.fittools import *
import re


class FitStruct:

    def __init__(self, ref_struct, fit_struct, correspondence_file=None, ignore_hydrogens=True):
        """

        Args:
            ref_struct:
            fit_struct:
            correspondence_file:
            ignore_hydrogens:
        """

        self._ref_atoms = ref_struct
        self._fit_atoms = fit_struct
        self._ignore_hydrogens = ignore_hydrogens

        self._ref_cor_atoms = None
        self._fit_cor_atoms = None
        self._corresponding_atoms = None
        self._corresponding_angles = None
        self._corresponding_dihedrals = None
        self.corresponding_residues = correspondence_file


    def _populate_corr_atoms_from_corr_residue(self, ref_residue, fit_residue, corr):
        """

        Args:
            ref_residue:
            fit_residue:
            corr:

        Returns:

        """
        for ref_atom in ref_residue.atoms:
            for fit_atom in fit_residue.atoms:
                if is_same_atom_name(ref_atom, fit_atom):
                    if not self._ignore_hydrogens:
                        corr[ref_atom] = fit_atom
                    elif self._ignore_hydrogens and not is_hydrogen(ref_atom) and not is_hydrogen(
                            fit_atom):
                        corr.atoms[ref_atom] = fit_atom

    @property
    def ref_cor_atoms(self):
        if self._ref_cor_atoms:
            return self._ref_cor_atoms
        else:
            self._ref_cor_atoms = MDAnalysis.core.groups.AtomGroup(list(self._corresponding_residues[0].atoms.keys()))
            for corr in self._corresponding_residues[1:]:
                self._ref_cor_atoms = self._ref_cor_atoms + MDAnalysis.core.groups.AtomGroup(list(corr.atoms.keys()))
            return self._ref_cor_atoms

    @property
    def fit_cor_atoms(self):
        if self._fit_cor_atoms:
            return self._fit_cor_atoms
        else:
            self._fit_cor_atoms = MDAnalysis.core.groups.AtomGroup(list(self._corresponding_residues[0].atoms.values()))
            for corr in self._corresponding_residues[1:]:
                self._fit_cor_atoms = self._fit_cor_atoms + \
                                      MDAnalysis.core.groups.AtomGroup(list(corr.atoms.values()))
            return self._fit_cor_atoms

    def _populate_corresponding_atoms(self):
        self._corresponding_atoms = {}
        for cor in self.corresponding_residues:
            self._corresponding_atoms.update(cor.atoms)

    def _populate_corresponding_angles(self):
        ref_angles = self.ref_cor_atoms.angles
        fit_angles = self.fit_cor_atoms.angles
        self._corresponding_angles = dict()
        for ref_angle in ref_angles:
            for fit_angle in fit_angles:
                temp_cor_angle_dict = {ref_atom: fit_atom for ref_atom, fit_atom in
                                       zip(ref_angle.atoms, fit_angle.atoms)}
                if temp_cor_angle_dict.items() <= self.corresponding_atoms.items():
                    self._corresponding_angles[ref_angle] = fit_angle


    @property
    def corresponding_atoms(self):
        if self._corresponding_atoms:
            return self._corresponding_atoms
        else:
            self._populate_corresponding_atoms()
            return self._corresponding_atoms

    @property
    def corresponding_angles(self):

        if not self._corresponding_angles:
            self._populate_corresponding_angles()
        return self._corresponding_angles

    @property
    def corresponding_dihedrals(self):

        if self._corresponding_dihedrals:
            return self._corresponding_dihedrals
        else:
            ref_angles = self.ref_cor_atoms.dihedrals
            fit_angles = self.fit_cor_atoms.dihedrals
            self._corresponding_dihedrals = dict()
            for ref_angle in ref_angles:
                for fit_angle in fit_angles:
                    temp_cor_angle_dict = {ref_atom: fit_atom for ref_atom, fit_atom in
                                           zip(ref_angle.atoms, fit_angle.atoms)}
                    if temp_cor_angle_dict.items() <= self.corresponding_atoms.items():
                        self._corresponding_dihedrals[ref_angle] = fit_angle
            return self._corresponding_dihedrals

    @property
    def corresponding_residues(self):
        return self._corresponding_residues

    @corresponding_residues.setter
    def corresponding_residues(self, file):

        CorrResidue = namedtuple("CorrResidue", ["ref_residue", "fit_residue", "atoms"])
        self._corresponding_residues = []

        try:
            open(file, 'r')
        except Exception:
            print(f'WARNING: Correspondence file {file} have not found or set!\n '
                  f'Will try to find similar atoms based on similar atom names, residue names and resiude numbers!')
            for ref_residue in self._ref_atoms.residues:
                for fit_residue in self._fit_atoms.residues:
                    if is_same_resid(ref_residue, fit_residue) and is_same_resname(ref_residue, fit_residue):
                        new_corr = CorrResidue(ref_residue, fit_residue, dict())
                        self._populate_corr_atoms_from_corr_residue(ref_residue, fit_residue, new_corr)
                        self._corresponding_residues.append(new_corr)

            return
        new_corr = ''
        is_successful = False
        with open(file, 'r') as f:
            while f:
                try:
                    line = next(f)
                    if line.strip().startswith('#') or not line.strip():
                        continue
                    elif line.startswith('RES'):
                        ref_resname_raw, fit_resname_raw = line.split()[1], line.split()[2]
                        ref_resid = re.search('^[0-9]+', ref_resname_raw).group()
                        ref_resname = re.search('[A-Z,a-z][A-Z,a-z,0-9]*', ref_resname_raw).group()
                        fit_resid = re.search('^[0-9]+', fit_resname_raw).group()
                        fit_resname = re.search('[A-Z,a-z][A-Z,a-z,0-9]*', fit_resname_raw).group()
                        print(ref_resname, ref_resid, fit_resname, fit_resid)
                        try:
                            ref_resid = \
                                self._ref_atoms.select_atoms(f'resname {ref_resname} and resid {ref_resid}').residues[0]
                            fit_resid = \
                                self._fit_atoms.select_atoms(f'resname {fit_resname} and resid {fit_resid}').residues[0]
                            new_corr = CorrResidue(ref_resid, fit_resid, dict())
                            self._corresponding_residues.append(new_corr)
                            is_successful = True
                        except IndexError:
                            print(
                                f"WARNING: Couldn't find reside named: "
                                f"reference residue: {ref_resname_raw} or fit_residue: {fit_resname_raw} ! Skipping...")
                            is_successful = False
                            continue

                    elif line.startswith('ATOM') and is_successful:
                        ref_atom_name = line.split()[1]
                        fit_atom_name = line.split()[2]

                        if self._ignore_hydrogens:
                            if is_hydrogen(ref_atom_name) or is_hydrogen(fit_atom_name):
                                continue

                        try:
                            ref_atom = \
                                new_corr.ref_residue.atoms.select_atoms(
                                    f'resname {new_corr.ref_residue.resname} and resid '
                                    f'{new_corr.ref_residue.resid} and name {ref_atom_name}')[
                                    0]
                            fit_atom = \
                                new_corr.fit_residue.atoms.select_atoms(
                                    f'resname {new_corr.fit_residue.resname} and resid '
                                    f'{new_corr.fit_residue.resid} and name {fit_atom_name}')[
                                    0]
                            new_corr.atoms[ref_atom] = fit_atom

                        except IndexError:
                            print(
                                f"WARNING: Couldn't find atom named: {ref_atom_name} in {new_corr.ref_residue.resid}"
                                f"{new_corr.ref_residue.resname} or"
                                f" {fit_atom_name} in {new_corr.fit_residue.resid}{new_corr.fit_residue.resname}"
                                f"! Skipping...")

                except StopIteration:
                    break

        # populating_residues with empty atoms by corresponding resname and resid
        for corr_residue in self._corresponding_residues:
            if not corr_residue.atoms:
                self._populate_corr_atoms_from_corr_residue(corr_residue.ref_residue, corr_residue.fit_residue,
                                                            corr_residue)
        self._populate_corresponding_atoms()

    @staticmethod
    def bonding_of_angles(angle_atomgroup):
        """Maps all the atoms which needs to be rotated related to the bond described by the angle."""

        atom_exclusion = set()
        bond_exclusion = set()
        atom_exclusion.update(angle_atomgroup[2:3])
        bonds = [bond for bond in angle_atomgroup.atoms[1].bonds if (angle_atomgroup.atoms[2] not in bond)]
        bond_exclusion.update(bonds)

        while True:
            new_atoms = get_atoms_from_bonds(bonds)
            atoms = [atom for atom in new_atoms if atom not in atom_exclusion]
            atom_exclusion.update(atoms)
            new_bonds = get_bonds_from_atoms(atoms)
            if new_bonds:
                bonds = [bond for bond in new_bonds if bond not in bond_exclusion]
            else:
                atom_exclusion = atom_exclusion - set(angle_atomgroup[2:3])
                break

        return MDAnalysis.core.groups.AtomGroup(list(atom_exclusion))

    @staticmethod
    def bonding_of_dihedral(dihedral_atomgroup):
        """Maps all the atoms which need to be rotated related to the bond described by the dihedral."""

        atom_exclusion = set()
        bond_exclusion = set()
        atom_exclusion.update(dihedral_atomgroup[2:4])
        bonds = [bond for bond in dihedral_atomgroup.atoms[1].bonds if
                 (dihedral_atomgroup.atoms[2] not in bond) and (dihedral_atomgroup.atoms[3] not in bond)]
        bond_exclusion.update(bonds)

        while True:
            new_atoms = get_atoms_from_bonds(bonds)
            atoms = [atom for atom in new_atoms if atom not in atom_exclusion]
            atom_exclusion.update(atoms)
            new_bonds = get_bonds_from_atoms(atoms)
            if new_bonds:
                bonds = [bond for bond in new_bonds if bond not in bond_exclusion]
            else:
                atom_exclusion = atom_exclusion - set(dihedral_atomgroup[2:4])
                break

        return mda.core.groups.AtomGroup(list(atom_exclusion))

    @staticmethod
    def filter_dihedrals_based_on_atomnames(corresponding_dihedral_pair, atomnames):
        """Decides whether a dihedral_pair consits of the exact atoms as given in atomnames list!"""

        fit_dihedral_atomgroup = corresponding_dihedral_pair[0]
        check_dihedral_atomgroup = [atom for atom in fit_dihedral_atomgroup.atoms if atom.name in atomnames]
        if len(fit_dihedral_atomgroup.atoms) == len(check_dihedral_atomgroup):
            return True
        else:
            return False

    def select_dihedrals_based_on_atoms_names(self, atom_names=('', '', '', '')):
        from itertools import compress
        print('Selecting dihedrals from fitting structure!')
        dihedrals = []
        atom_names_exist = tuple(compress(atom_names, [bool(item) for item in atom_names]))
        for dihedral in self._fit_atoms.atoms.dihedrals:
            dihed_names = tuple(compress(dihedral.atoms.names, [bool(item) for item in atom_names]))
            if atom_names_exist == dihed_names or atom_names_exist[::-1] == dihed_names:
                dihedrals.append(dihedral)
        print('dihedrals selected: \n')
        for item in dihedrals:
            print(item.atoms.names)
        return dihedrals

    def fit_angles(self, align=False):
        for ref_angle, fit_angle in self.corresponding_angles.items():
            structure_atoms = self.bonding_of_angles(fit_angle.atoms)
            atom1 = fit_angle.atoms[0]
            atom2 = fit_angle.atoms[1]
            atom3 = fit_angle.atoms[2]
            target_angle = np.deg2rad(ref_angle.angle() - fit_angle.angle())
            center_coordsystem_on_atom(self._fit_atoms, atom2)
            modifiy_angle(structure_atoms, atom1, atom2, atom3, target_angle)
            if align:
                self.align()

    def fit_dihedrals(self, align=False):
        for ref_dihedral, fit_dihedral in self.corresponding_dihedrals.items():
            structure_atoms = self.bonding_of_dihedral(fit_dihedral.atoms)
            bond_atom1 = fit_dihedral.atoms[1]
            bond_atom2 = fit_dihedral.atoms[2]
            ref_dihedral_angle = calculate_dihedral_angle(ref_dihedral.atoms)
            current_dihedral_angle = calculate_dihedral_angle(fit_dihedral.atoms)

            target_dihedral = np.deg2rad(current_dihedral_angle - ref_dihedral_angle)
            center_coordsystem_on_atom(self._fit_atoms, bond_atom1)
            modifiy_dihedral(structure_atoms, bond_atom1, bond_atom2, target_dihedral)
            if align:
                self.align()

    def set_dihedral(self, dihedral, angle, align=True):
        """

        Args:
            dihedral: A dihedral object from corresponding dihedrals
            angle: target angle in degrees!

        Returns:
            None
        """
        if dihedral not in self._fit_atoms.dihedrals:
            raise ValueError("Couldn't find the dihedral inside the fit_atoms!")

        elif dihedral not in self.corresponding_dihedrals.values():
            print('WARNING: Setting a dihedral which does not have a corresponding reference dihedral!')

        structure_atoms = self.bonding_of_dihedral(dihedral.atoms)
        bond_atom1 = dihedral.atoms[1]
        bond_atom2 = dihedral.atoms[2]
        current_dihedral_angle = calculate_dihedral_angle(dihedral.atoms)
        target_dihedral = np.deg2rad(current_dihedral_angle - angle)
        center_coordsystem_on_atom(self._fit_atoms.atoms, bond_atom1)
        modifiy_dihedral(structure_atoms, bond_atom1, bond_atom2, target_dihedral)
        if align:
            self.align()


    def align(self):
        """

        Returns:

        """
        selected_fit_atoms = 'bynum ' + " ".join(
            [str(fit_atom.id) for ref_atom, fit_atom in self._corresponding_atoms.items() if
             ref_atom.name == fit_atom.name])
        fit_atoms = [str(fit_atom) for ref_atom, fit_atom in self._corresponding_atoms.items() if
         ref_atom.name == fit_atom.name]

        selected_ref_atoms = 'bynum ' + " ".join(
            [str(ref_atom.id) for ref_atom, fit_atom in self._corresponding_atoms.items() if
             ref_atom.name == fit_atom.name])
        ref_atoms = [str(ref_atom) for ref_atom, fit_atom in self._corresponding_atoms.items() if
         ref_atom.name == fit_atom.name]
        for key, value in self._corresponding_atoms.items():
            print(key, value)
        align.alignto(self._fit_atoms, self._ref_atoms, select=(selected_fit_atoms, selected_ref_atoms), strict=False)

    def write(self, filename):
        self._fit_atoms.write(filename)
