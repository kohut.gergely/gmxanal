

corr_file2 = '/media/kohut/TAR/Amber/diosbulbin_B/mutation_sim/384/align/atp_mg/corresponding_residues_atp_wo.txt'

fit_tpr_file = '/media/kohut/TAR/Amber/diosbulbin_B/ref_struct_params/atp/top.top'
fit_struct_file = '/media/kohut/TAR/Amber/diosbulbin_B/ref_struct_params/atp/init.inpcrd'
corr_file = '/media/kohut/TAR/Amber/diosbulbin_B/mutation_sim/384/align/atp_mg/corresponding_residues_atp.txt'

fit_tpr_file3 = '/media/kohut/TAR/Amber/diosbulbin_B/ref_struct_params/diosbulbin_B/dio.top'
fit_struct_file3 = '/media/kohut/TAR/Amber/diosbulbin_B/ref_struct_params/diosbulbin_B/dio.inpcrd'
corr_file3 = '/media/kohut/TAR/Amber/diosbulbin_B/mutation_sim/384/align/open/new.txt'

ref_tpr_file = '/media/kohut/TAR/Amber/diosbulbin_B/ref_struct_params/2jdi/anp_mg/top.top'
ref_struct_file = '/media/kohut/TAR/Amber/diosbulbin_B/ref_struct_params/2jdi/anp_mg/init.inpcrd'

fit_struct = MDAnalysis.Universe(fit_tpr_file3, fit_struct_file3)
ref_struct = MDAnalysis.Universe(ref_tpr_file, ref_struct_file)
cc = FitStruct(ref_struct.atoms, fit_struct.atoms, corr_file3)
print(cc.corresponding_atoms)
cc.fit_angles()
cc.fit_dihedrals()
cc.align()
cc.write('new.pdb')

fit_struct = MDAnalysis.Universe(fit_tpr_file, fit_struct_file)
ref_struct = MDAnalysis.Universe(ref_tpr_file, ref_struct_file)
cc = FitStruct(ref_struct.atoms, fit_struct.atoms, corr_file)
print(cc.corresponding_atoms)
cc.fit_angles()
cc.fit_dihedrals()
cc.align()
cc.write('new2.pdb')
