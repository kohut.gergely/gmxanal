import numpy as np
import math
from MDAnalysis.analysis import align
import MDAnalysis as mda
from MDAnalysis.analysis.dihedrals import Dihedral
import copy

def rotation_matrix(axis, theta):
    """
     Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.

    Args:
        axis (np.array): axis vector of the rotation!
        theta (Number): angle to rotate in radians

    Returns:
        (np.array): rotation matrix!
    """

    axis = np.asarray(axis)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


def normalize(v):
    """
    Returns the normalized vector!

    Args:
        v (np.array): numpy array

    Returns
        np.array: normalized vector

    """
    norm = np.linalg.norm(v)
    if norm == 0:
        return v
    return v / norm


def calc_angle_between_vectors(u, v):
    """
     Calculates the angle between two vectors in degrees!
    Args:
        u (np.array) : vector1
        v (np.array) : vector2

    Returns: angle between them in degrees

    """

    from numpy.linalg import norm

    cos_angl = np.dot(u, v) / norm(u) / norm(v)
    return np.degrees(np.arccos(np.clip(cos_angl, -1, 1)))


def align_two_structures(fit_struct, reference_struct, selection):
    """
    Superimpose two structures on each other, based on a mutual selection!
    Args:
        fit_struct          (MDAnalysis.core.groups.AtomGroup):     AtomGroup of the structure to be fit!
        reference_struct    (MDAnalysis.core.groups.AtomGroup):     AtomGroup of the reference structure!
        selection           (str):                                  MDAnalysis based selection of the mutual atoms to
                                                                    be used!

    Returns:
        NoneType
    """

    align.alignto(reference_struct, fit_struct, select=selection, weights=None)


def center_coordsystem_on_atom(structure, target_atom):
    """
    Centers the coordinate system on a target atom of the given molecule!

    Args:
        structure:          (MDAnalysis.core.groups.AtomGroup):     AtomGroup of the structure to be fit!
        target_atom:        (MDAnalysis.core.groups.Atom):          Target Atom which the coordinate system will be
                                                                    centered on!

    Returns:
        NoneType

    """

    ref_coord = target_atom.position.copy()
    for atom in structure:
        atom.position = atom.position - ref_coord


def rotate_molecule_bond_to_fit_axis(structure, atom1, atom2, coincidence_vector=None):
    """
    Rotates the molecule in a way that the bond described by atom1 and atom2 is aligned with
    given axis vector!
    Args:
        structure           (MDAnalysis.core.groups.AtomGroup):     AtomGroup to be rotated!
        atom1               (MDAnalysis.core.groups.Atom):          Atom1 of the target bond
        atom2               (MDAnalysis.core.groups.Atom):          Atom2 of the target bond
        coincidence_vector: (np.array):                             Vector which the bond should be aligned!
                                                                    Default is x axis!

    Returns:
        NoneType
    """

    if not coincidence_vector:
        coincidence_vector = np.array([1, 0, 0])

    bond_vector = normalize(atom2.position - atom1.position)
    axis_vector = (coincidence_vector + bond_vector) / 2.0
    rot_matrix = rotation_matrix(axis_vector, np.pi)
    for atom in structure.atoms:
        atom.position = np.matmul(rot_matrix, atom.position)


def save_structure(structure, file_name):
    """
    Saves the target structure to file!
    Args:
        structure           (MDAnalysis.core.groups.AtomGroup):     AtomGroup to be saved!
        file_name           (str):                                  Filename

    Returns:
        NoneType
    """

    with mda.Writer(file_name, structure) as W:
        W.write(structure)


def calculate_dihedral_angle(atomgroup):
    """
    Calculates the dihedral angle of an atomgroup with four atoms!
    Args:
        atomgroup           (MDAnalysis.core.groups.AtomGroup):     AtomGroup with 4 atoms

    Returns:
        float:              dihedral angle in radians!
    """

    try:
        atg = [copy.deepcopy(atomgroup)]
        return Dihedral(atg).run().angles[0][0]
    except ValueError:
        print('AtomGroup must contain exactly four atoms!')
        raise ValueError


def modifiy_dihedral(atomgroup, bond_atom1, bond_atom2, target_angle):
    """

    Args:
        atomgroup           (MDAnalysis.core.groups.AtomGroup):     AtomGroup to be modified!
        bond_atom1          (MDAnalysis.core.groups.Atom):          Atom1 of the target bond
        bond_atom2          (MDAnalysis.core.groups.Atom):          Atom2 of the target bond
        target_angle:       (float):                                Target angle in radians

    Returns:

    """
    axis_vector = normalize(bond_atom2.position - bond_atom1.position)
    rot_matrix = rotation_matrix(axis_vector, target_angle)
    for atom in atomgroup.atoms:
        atom.position = np.matmul(rot_matrix, atom.position)


def modifiy_angle(atomgroup, atom1, atom2, atom3, target_angle):
    """

    Args:
        atomgroup:          (MDAnalysis.core.groups.AtomGroup):     AtomGroup to be modified!
        atom1:              (MDAnalysis.core.groups.Atom):
        atom2:              (MDAnalysis.core.groups.Atom):
        atom3:              (MDAnalysis.core.groups.Atom):
        target_angle:       (float):                                Target angle in radians

    Returns:

    """
    vector1 = normalize(atom2.position - atom1.position)
    vector2 = normalize(atom3.position - atom2.position)
    axis_vector = np.cross(vector1, vector2)
    rot_matrix = rotation_matrix(axis_vector, target_angle)
    for atom in atomgroup.atoms:
        atom.position = np.matmul(rot_matrix, atom.position)

def get_atoms_from_bonds(bonds):
    """Gives back the corresponding atoms of a list of bonds"""

    atoms = []
    for bond in bonds:
        for atom in bond.atoms:
            atoms.append(atom)
    return atoms


def get_bonds_from_atoms(atoms):
    """Gives back the corresponding bonds of a list of atoms"""

    bonds = []
    for atom in atoms:
        for bond in atom.bonds:
            bonds.append(bond)
    return bonds

def is_hydrogen(atom):
    """

    Args:
        atom            (Optional):

    Returns:
        bool:

    """
    if isinstance(atom, str):
        return atom.lower().startswith('h')
    elif isinstance(atom, mda.core.groups.Atom):
        return atom.name.lower().startswith('h')
    else:
        raise ValueError(f'{atom} must be of string or mda.core.groups.Atom instead of {type(atom)}!')

def is_same_resname(atom1,atom2):
    """

    Args:
        atom            (Optional):

    Returns:
        bool:

    """
    if isinstance(atom1,str) and isinstance(atom2,str):
        return atom1.lower() == atom2.lower()
    if isinstance(atom1, mda.core.groups.Atom) and isinstance(atom2, mda.core.groups.Atom):
        return atom1.name.lower() == atom2.name.lower()
    if isinstance(atom1, mda.core.groups.Residue) and isinstance(atom2, mda.core.groups.Residue):
        return atom1.resname == atom2.resname
    else:
        raise ValueError(f'atom1 and atom2 must be of same types and either string'
                         f'or MDAnalysis.core.group.Atom!\n Type of {atom1}: {type(atom1)} and {atom2}: {type(atom2)} are different!')


def is_same_resid(atom1,atom2):
    """

    Args:
        atom            (Optional):

    Returns:
        bool:

    """
    if (isinstance(atom1, int) and isinstance(atom2, int)) or (isinstance(atom1, str) and isinstance(atom2, str)):
        return atom1 == atom2
    if isinstance(atom1, mda.core.groups.Atom) and isinstance(atom2, mda.core.groups.Atom):
        return atom1.resid == atom2.resid
    if isinstance(atom1, mda.core.groups.Residue) and isinstance(atom2, mda.core.groups.Residue):
        return atom1.resid == atom2.resid
    else:
        raise ValueError(f'atom1 and atom2 must be of same types and either string, int'
                         f'or MDAnalysis.core.group.Atom!\n Type of {atom1}: {type(atom1)} and {atom2}: {type(atom2)} are different!')

def is_same_atom_name(atom1,atom2):
    """

    Args:
        atom            (Optional):

    Returns:
        bool:

    """
    if isinstance(atom1, str) and isinstance(atom2, str):
        return atom1 == atom2
    if isinstance(atom1, mda.core.groups.Atom) and isinstance(atom2, mda.core.groups.Atom):
        return atom1.name == atom2.name
    else:
        raise ValueError(f'atom1 and atom2 must be of same types and either string'
                         f'or MDAnalysis.core.group.Atom!\n Type of {atom1}: {type(atom1)} and {atom2}: {type(atom2)} are different!')

def replace_residue(ref_struct, target_struct, ref_selection = None, target_selection = None):
    target_struct = mda.Merge(target_struct).atoms
    ref_struct = mda.Merge(ref_struct).atoms
    if ref_selection and target_selection:
        replace_atoms = ref_struct.select_atoms(ref_selection)
        target_atoms = target_struct.select_atoms(target_selection)

    elif ref_selection and not target_selection:
        replace_atoms = ref_struct.select_atoms(ref_selection)   
        target_atoms = target_struct
    elif target_selection and not ref_selection:
        replace_atoms = ref_struct
        target_atoms = target_struct.select_atoms(target_selection)

    elif not target_selection and not ref_selection:
        target_atoms = target_struct
        replace_atoms = ref_struct

    assert len(target_atoms.residues) == len(replace_atoms.residues), ValueError('number of target and replacable residues must be equal!')
    pairs = {rep_res:targ_res for rep_res,targ_res in zip(replace_atoms.residues,target_atoms.residues)}
    list_of_residues = []
    for residue in ref_struct.residues:
        if residue in pairs.keys():
            pairs[residue].resid = residue.resid
            list_of_residues.append(pairs[residue].atoms)
        else:
            list_of_residues.append(residue.atoms)
    return mda.Merge(*list_of_residues)


