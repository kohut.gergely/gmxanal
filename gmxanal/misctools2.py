import MDAnalysis
import networkx as nx
import numpy as np
from collections import namedtuple
from copy import deepcopy
from MDAnalysis.lib.util import cached, warn_if_not_unique, unique_int_1d
class MoleculeTypeWarning(Exception):
    pass


class GraphCreationError(Exception):
    """Error related to graph creation from the bonding system of a given selection!"""
    pass


class Molecule(MDAnalysis.core.groups.AtomGroup):
    """Creates a Molecule class based on an atomgroup, having the same functionality as the AtomGroup class!"""

    def __init__(self, *args):
        try:
            if len(args) == 1:
                # list of atoms/res/segs, old init method
                ix = [at.ix for at in args[0]]
                u = args[0][0].universe
            else:
                # current/new init method
                ix, u = args
        except (AttributeError,  # couldn't find ix/universe
                TypeError):  # couldn't iterate the object we got
            raise TypeError(
                "Can only initialise a Group from an iterable of Atom/Residue/"
                "Segment objects eg: AtomGroup([Atom1, Atom2, Atom3]) "
                "or an iterable of indices and a Universe reference "
                "eg: AtomGroup([0, 5, 7, 8], u).")

        # indices for the objects I hold
        self._ix = np.asarray(ix, dtype=np.intp)
        self._u = u
        self._cache = dict()
        self._name ='bela'


    def name(self):
        return f'{self._name}'

    @property
    def label(self):
        return f'{self.name}_{self.ix}'



class MoleculeGroup(MDAnalysis.core.groups.GroupBase):
    """DCT"""



def molecule_parser(universe, basename='Molecule', selection='all'):

    counter = 0
    molecules = []
    atomgroup = universe.select_atoms(selection)
    try:
        new_graph = nx.Graph(list(atomgroup.bonds.to_indices()))
    except AttributeError:
        raise GraphCreationError('Topology file must be loaded to use this function!')
    subgraphs = [new_graph.subgraph(c).copy() for c in nx.connected_components(new_graph)]
    for molecule in subgraphs:
        atom_indices = molecule.nodes
        atomgrp = [atom for atom in sorted(universe.atoms[atom_indices])]
        molecules.append(Molecule(atomgrp))
        counter += 1
    return MoleculeGroup(molecules)


def calculate_distance_between_two_atomgroups(self, other, _type='abs'):

    if isinstance(other, Molecule) or isinstance(other, MDAnalysis.core.groups.AtomGroup):
        com_self = self.center_of_mass()
        com_other = other.center_of_mass()
    else:
        raise TypeError('other should be of type Molecule or AtomGroup!')
    universe = self.universe
    if _type.lower() == 'abs':
        return MDAnalysis.lib.distances.calc_bonds(com_other, com_self, box=universe.dimensions)
    elif _type.lower() == 'x':
        return MDAnalysis.lib.distances.calc_bonds(com_other[0], com_self[0], box=universe.dimensions)
    elif _type.lower() == 'y':
        return MDAnalysis.lib.distances.calc_bonds(com_other[1], com_self[1], box=universe.dimensions)
    elif _type.lower() == 'z':
        return MDAnalysis.lib.distances.calc_bonds(com_other[2], com_self[2], box=universe.dimensions)


def helper_find_dir_fname_ext(file_path):
    """Helper function to decompose a file_path path to its: directory, file_path and extention!"""
    import os

    if file_path.startswith('/'):
        directory = '/'.join(file_path.split('/')[:-1]) + '/'
        try:
            fname, ext = file_path.split('/')[-1].split('.')[-2:]
        except ValueError:
            fname, ext = file_path.split('/')[-1].split('.')[-1], None

    elif file_path.startswith('~'):
        file_path = file_path[2:]
        home = os.environ['HOME']
        directory = f'{home}/' + '/'.join(file_path.split('/')[:-1]) + '/'
        try:
            fname, ext = file_path.split('/')[-1].split('.')[-2:]
        except ValueError:
            fname, ext = file_path.split('/')[-1].split('.')[-1], None
    else:
        directory = ''
        try:
            fname, ext = file_path.split('.')[-2:]
        except ValueError:
            fname, ext = file_path.split('.')[-1], None
    if ext:
        ext = f'.{ext}'
    else:
        ext = ''
    return directory, fname, ext


def write_index_group(atomgroup, groupname='groupname', filename='atomgroup.ndx', write_option='w+'):
    """Writes an MDAnalysis atomgroup to a given index file!
     Write options are the standard options of python's open() function! E.g: 'w, w+, r, a, a+'"""

    with open(filename, write_option) as file:
        file.write('[ ' + groupname + ' ]')
        counter = 0
        for atom in atomgroup:
            if counter % 15 == 0:
                file.write('\n')
            if atom.id < 10000:
                file.write("{0:>4} ".format(str(atom.id + 1)))
            elif atom.id >= 10000 and atom.id < 100000:
                file.write("{0:>4} ".format(str(atom.id + 1)))
            elif atom.id > 100000:
                file.write("{0:>4} ".format(str(atom.id + 1)))
            counter += 1
        file.write('\n')


def create_index_files(trajectory_filepath,
                       tpr_filepath,
                       selections=None, find_leaflet=False,
                       bilayer_selection='resname DOPC', output_filename='index_surface.ndx'):
    """Creates index files from a structure/trajectory file, based on MDAnalysis selections!
        If find_leaflet is True, it defines the leaflet atom as well, the selection of the membrane should be given by
        the bilayer selection keyword argument. Important: the leaflets are defined through their phosphorous atoms!"""

    import MDAnalysis.analysis.leaflet
    if not selections:
        selections = {'protein': 'protein'}
    if not isinstance(selections, dict):
        raise TypeError('selections should be a dictionary if a format: {index_name:selection}')
    directory, fname, ext = helper_find_dir_fname_ext(trajectory_filepath)
    u = MDAnalysis.Universe(tpr_filepath, trajectory_filepath)
    selected_atomgroups = {index_group_name: u.select_atoms(selection) for index_group_name, selection in
                           selections.items()}
    if find_leaflet:
        leaflet = MDAnalysis.analysis.leaflet.LeafletFinder(u, f'{bilayer_selection} and name P*')
        top_leaflet = leaflet.groups(0)
        bot_leaflet = leaflet.groups(1)
        selected_atomgroups['top_leaflet'] = top_leaflet
        selected_atomgroups['bottom_leaflet'] = bot_leaflet

    open(f'{directory}{output_filename}', 'w')

    for index_group_name, atomgroup in selected_atomgroups.items():
        write_index_group(atomgroup, groupname=index_group_name, filename=f'{directory}{output_filename}',
                          write_option='a')


def read_gmx_dssp_count_file_to_pandas(file_path):
    """Reads a count.xvg file created by GROMACS do_dssp command into a pandas dataframe!"""

    import subprocess
    import re as re
    import pandas as pd

    directory, fname, ext = helper_find_dir_fname_ext(file_path)
    names = []
    lines_to_keep = []
    with open(f'{directory}{fname}{ext}', 'r') as file:
        for line in file.readlines():
            if line.startswith('#') or line.startswith('@'):
                if line.startswith('@'):
                    res = re.match('^@ s[0-9]* legend \".*"', line)
                    if res:
                        names.append(res.group(0).split()[-1][1:-1])
            else:
                lines_to_keep.append(line)
    with open(f'{directory}{fname}_processed{ext}', 'w') as file:
        for item in lines_to_keep:
            file.write(item)
    data = pd.read_csv(f'{directory}{fname}_processed{ext}',
                       names=names,
                       sep=r'\s+')
    subprocess.run(f'rm {directory}{fname}_processed{ext}', shell=True)
    return data


def read_gmx_dist_xyz_file_to_pandas(file_path):
    """Reads an xyz type file created by GROMACS distance, mindist etc. command into a pandas dataframe!"""

    import subprocess
    import pandas as pd

    directory, fname, ext = helper_find_dir_fname_ext(file_path)
    names = []
    lines_to_keep = []
    with open(f'{directory}{fname}{ext}', 'r') as file:
        for line in file.readlines():
            if not (line.startswith('#') or line.startswith('@')):
                lines_to_keep.append(line)
    number_of_columns = len(lines_to_keep[0].split()) - 1
    for index in range(1, int(number_of_columns / 3) + 1):
        names.append(f'X{str(index)}')
        names.append(f'Y{str(index)}')
        names.append(f'Z{str(index)}')
    with open(f'{directory}{fname}_processed{ext}', 'w') as file:
        for item in lines_to_keep:
            file.write(item)
    data = pd.read_csv(f'{directory}{fname}_processed{ext}',
                       names=names,
                       sep=r'\s+')
    subprocess.run(f'rm {directory}{fname}_processed{ext}', shell=True)
    return data


def read_gmx_dist_av_file_to_pandas(file_path):
    """Reads an avg type file created by GROMACS distance, mindist etc. command into a pandas dataframe!"""
    import subprocess
    import pandas as pd

    directory, fname, ext = helper_find_dir_fname_ext(file_path)
    names = []
    lines_to_keep = []
    with open(f'{directory}{fname}{ext}', 'r') as file:
        for line in file.readlines():
            if not (line.startswith('#') or line.startswith('@')):
                lines_to_keep.append(line)
    number_of_columns = len(lines_to_keep[0].split()) - 1
    for index in range(1, int(number_of_columns) + 1):
        names.append(f'av_dist{str(index)}')
        with open(f'{directory}{fname}_processed{ext}', 'w') as file:
            for item in lines_to_keep:
                file.write(item)
    data = pd.read_csv(f'{directory}{fname}_processed{ext}',
                       names=names,
                       sep=r'\s+')
    subprocess.run(f'rm {directory}{fname}_processed{ext}', shell=True)
    return data


def gmx_execute(command, **kwargs):
    """Executes a gmx command. Positional argument: the gmx command to exectue. Flags and parameters are keyword args.
    E.g.: f=traj.xtc,s=traj.tpr,v=True"""
    import subprocess

    cmd = f'gmx {str(command)} '
    for key, value in kwargs.items():
        if isinstance(value, bool):
            cmd += f'-{str(value)} '
        else:
            cmd += f'-{str(key)} {str(value)} '
    return subprocess.run(cmd, shell=True, capture_output=True)


_Level = namedtuple('Level', ['name', 'singular', 'plural'])

MOLECULELEVEL = _Level('molecule', Molecule, MoleculeGroup)


Molecule.level = MOLECULELEVEL
MoleculeGroup.level = MOLECULELEVEL
