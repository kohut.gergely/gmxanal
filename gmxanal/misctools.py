import MDAnalysis
import networkx as nx
import numpy as np
from copy import deepcopy


class MoleculeTypeWarning(Exception):
    pass


class GraphCreationError(Exception):
    """Error related to graph creation from the bonding system of a given selection!"""
    pass


class Molecule(MDAnalysis.core.groups.AtomGroup):
    """Creates a Molecule class based on an atomgroup, having the same functionality as the AtomGroup class!"""

    def __init__(self, atomgroup, id_, name):
        super().__init__(atomgroup)
        self._name = name
        self._id = id_
        self._nearest_neighbours = None

    @property
    def name(self):
        return f'{self._name}'

    @property
    def id(self):
        return self._id

    @property
    def label(self):
        return f'{self.name}_{self.id}'

    @property
    def atom_labels(self, show=True):
        s = '\n'.join(f'{self.label}:{atom.name}' for atom in self)
        if show:
            print(s)
        return s

    @property
    def nearest_neighbours(self):
        if self._nearest_neighbours:
            return self._nearest_neighbours
        else:
            print('Warning: Nearest neighbours is None! It can be calculated through a MoleculeGroup only!')
            return self._nearest_neighbours

    def distance_com(self, other, _type='abs'):

        if isinstance(other, Molecule) or isinstance(other, MDAnalysis.core.groups.AtomGroup):
            com_self = self.center_of_mass()
            com_other = other.center_of_mass()
        else:
            raise TypeError('other should be of type Molecule or AtomGroup!')
        universe = self.universe
        if _type.lower() == 'abs':
            return MDAnalysis.lib.distances.calc_bonds(com_other, com_self, box=universe.dimensions)
        elif _type.lower() == 'x':
            return MDAnalysis.lib.distances.calc_bonds(com_other[0], com_self[0], box=universe.dimensions)
        elif _type.lower() == 'y':
            return MDAnalysis.lib.distances.calc_bonds(com_other[1], com_self[1], box=universe.dimensions)
        elif _type.lower() == 'z':
            return MDAnalysis.lib.distances.calc_bonds(com_other[2], com_self[2], box=universe.dimensions)

    def distance_pairwise_atomic(self, other, _type='abs'):

        if isinstance(other, Molecule) or isinstance(other, MDAnalysis.core.groups.AtomGroup):
            if _type.lower() == 'abs':
                universe = self.universe
                return MDAnalysis.lib.distances.distance_array(self.positions, other.positions, box=universe.dimensions)
            else:
                return None
        else:
            raise TypeError('other should be of type Molecule or AtomGroup!')


class MoleculeGroup:

    def __init__(self, molecules, name):

        try:
            self.molecules = []
            if isinstance(molecules, list) or isinstance(molecules, tuple):
                for molecule in molecules:
                    if isinstance(molecule, Molecule):
                        self.molecules.append(molecule)
                    else:
                        raise MoleculeTypeWarning(f'Skipping: type of {molecule} is not Molecule!')
        except MoleculeTypeWarning as ex:
            print(ex.args[0])
        if len(self.molecules) < 2:
            raise ValueError(f'Error: length of MoleculeGroup must be higher than 1!')
        self._name = name
        self._neighbour_number = 5
        self._clusters = []

    def __len__(self):

        return len(self.molecules)

    def __repr__(self):

        return f'<{self.__class__.__name__} with {len(self.molecules)} molecules>'

    def __getitem__(self, value):

        return self.molecules[value]

    def __add__(self, other):

        if isinstance(other, MoleculeGroup):
            return MoleculeGroup(self.molecules+other.molecules, f'{self.name}+{other.name}')
        raise TypeError(f'{other} must be MoleculeGroup')

    def __iadd__(self, other):

        if isinstance(other, MoleculeGroup):
            self.molecules += other.molecules
            return self
        else:
            raise TypeError(f'{other} must be MoleculeGroup')

    def __deepcopy__(self, memodict={}):

        new_universe = self.molecules[0].universe.copy()
        print(new_universe.dimens)
        new_moleculegroup = []
        for item in self.molecules:
            new_molecule = deepcopy(item)
            new_moleculegroup.append(new_molecule)
        return MoleculeGroup(new_moleculegroup, self.name)

    @property
    def name(self):
        return f'{self._name}'

    @property
    def neighbour_number(self):
        return self._neighbour_number

    @neighbour_number.setter
    def neighbour_number(self, value):
        if not isinstance(value, int):
            raise TypeError('Number of neighbours must be an integer!')
        if value > 0 and len(self.molecules) >= value:
            self._neighbour_number = value
        elif value > 0 and len(self.molecules) < value:
            print(f'Warning:{self.__repr__()} smaller then {value}! neighbour_number set to {self.__len__()}')
            self._neighbour_number = value
        else:
            raise ValueError('Number of neighbours must be higher than 0!')

    @property
    def clusters(self):
        if self._clusters:
            return self._clusters
        return None

    def nearest_neighbours(self):

        dist_mat = self.distance_matrix()
        closest = np.argsort(dist_mat, axis=1)
        for index, molecule in enumerate(self.molecules):
            molecule._nearest_neighbours = [self.molecules[idx] for idx in closest[index, 1:self.neighbour_number+1]]
        return closest[:, 1:self.neighbour_number+1]

    def distance_matrix(self, distance_type='com'):

        if distance_type.lower() == 'com':
            universe = self.molecules[0].universe
            coms = np.array([item.center_of_mass() for item in self.molecules])
            return MDAnalysis.lib.distances.distance_array(coms, coms, box=universe.dimensions)

    def cluster_molecules(self, function=None, default=True, **cluster_options):
        from sklearn.cluster import DBSCAN
        from itertools import groupby

        if default:
            function = DBSCAN
            cluster_options = {'eps': 15.0, 'min_samples': 2, 'metric': "precomputed"}
        model = function(**cluster_options)
        fit = model.fit_predict(self.distance_matrix())
        keyfunc = lambda item: item[1]
        data = sorted(zip(self.molecules, fit), key=keyfunc)
        groups = [list(g) for k, g in groupby(data, key=keyfunc)]
        for group in groups[1:]:
            group_idx = group[0][1]
            molecules = [molecule for molecule, group_idx in group]
            try:
                new_molecule_group = MoleculeGroup(molecules, f'clust{group_idx}')
                self._clusters.append(new_molecule_group)
            except ValueError as exp:
                print(exp)
                continue
        return self.clusters


def molecule_parser(universe, basename='Molecule', selection='all'):

    counter = 0
    molecules = []
    atomgroup = universe.select_atoms(selection)
    try:
        new_graph = nx.Graph(list(atomgroup.bonds.to_indices()))
    except AttributeError:
        raise GraphCreationError('Topology file must be loaded to use this function!')
    subgraphs = [new_graph.subgraph(c).copy() for c in nx.connected_components(new_graph)]
    for molecule in subgraphs:
        atom_indices = molecule.nodes
        molecules.append(Molecule(sorted(universe.atoms[atom_indices], key=lambda item: item.id), counter, basename))
        counter += 1
    return MoleculeGroup(molecules, basename)


def calculate_distance_between_two_atomgroups(self, other, _type='abs'):

    if isinstance(other, Molecule) or isinstance(other, MDAnalysis.core.groups.AtomGroup):
        com_self = self.center_of_mass()
        com_other = other.center_of_mass()
    else:
        raise TypeError('other should be of type Molecule or AtomGroup!')
    universe = self.universe
    if _type.lower() == 'abs':
        return MDAnalysis.lib.distances.calc_bonds(com_other, com_self, box=universe.dimensions)
    elif _type.lower() == 'x':
        return MDAnalysis.lib.distances.calc_bonds(com_other[0], com_self[0], box=universe.dimensions)
    elif _type.lower() == 'y':
        return MDAnalysis.lib.distances.calc_bonds(com_other[1], com_self[1], box=universe.dimensions)
    elif _type.lower() == 'z':
        return MDAnalysis.lib.distances.calc_bonds(com_other[2], com_self[2], box=universe.dimensions)


def helper_find_dir_fname_ext(file_path):
    """Helper function to decompose a file_path path to its: directory, file_path and extention!"""
    import os

    if file_path.startswith('/'):
        directory = '/'.join(file_path.split('/')[:-1]) + '/'
        try:
            fname, ext = file_path.split('/')[-1].split('.')[-2:]
        except ValueError:
            fname, ext = file_path.split('/')[-1].split('.')[-1], None

    elif file_path.startswith('~'):
        file_path = file_path[2:]
        home = os.environ['HOME']
        directory = f'{home}/' + '/'.join(file_path.split('/')[:-1]) + '/'
        try:
            fname, ext = file_path.split('/')[-1].split('.')[-2:]
        except ValueError:
            fname, ext = file_path.split('/')[-1].split('.')[-1], None
    else:
        directory = ''
        try:
            fname, ext = file_path.split('.')[-2:]
        except ValueError:
            fname, ext = file_path.split('.')[-1], None
    if ext:
        ext = f'.{ext}'
    else:
        ext = ''
    return directory, fname, ext


def write_index_group(atomgroup, groupname='groupname', filename='atomgroup.ndx', write_option='w+'):
    """Writes an MDAnalysis atomgroup to a given index file!
     Write options are the standard options of python's open() function! E.g: 'w, w+, r, a, a+'"""

    with open(filename, write_option) as file:
        file.write('[ ' + groupname + ' ]')
        counter = 0
        for atom in atomgroup:
            if counter % 15 == 0:
                file.write('\n')
            if atom.id < 10000:
                file.write("{0:>4} ".format(str(atom.id + 1)))
            elif atom.id >= 10000 and atom.id < 100000:
                file.write("{0:>4} ".format(str(atom.id + 1)))
            elif atom.id > 100000:
                file.write("{0:>4} ".format(str(atom.id + 1)))
            counter += 1
        file.write('\n')


def create_index_files(trajectory_filepath,
                       tpr_filepath,
                       selections=None, find_leaflet=False,
                       bilayer_selection='resname DOPC', output_filename='index_surface.ndx'):
    """Creates index files from a structure/trajectory file, based on MDAnalysis selections!
        If find_leaflet is True, it defines the leaflet atom as well, the selection of the membrane should be given by
        the bilayer selection keyword argument. Important: the leaflets are defined through their phosphorous atoms!"""

    import MDAnalysis.analysis.leaflet
    if not selections:
        selections = {'protein': 'protein'}
    if not isinstance(selections, dict):
        raise TypeError('selections should be a dictionary if a format: {index_name:selection}')
    directory, fname, ext = helper_find_dir_fname_ext(trajectory_filepath)
    u = MDAnalysis.Universe(tpr_filepath, trajectory_filepath)
    selected_atomgroups = {index_group_name: u.select_atoms(selection) for index_group_name, selection in
                           selections.items()}
    if find_leaflet:
        leaflet = MDAnalysis.analysis.leaflet.LeafletFinder(u, f'{bilayer_selection} and name P*')
        top_leaflet = leaflet.groups(0)
        bot_leaflet = leaflet.groups(1)
        selected_atomgroups['top_leaflet'] = top_leaflet
        selected_atomgroups['bottom_leaflet'] = bot_leaflet

    open(f'{directory}{output_filename}', 'w')

    for index_group_name, atomgroup in selected_atomgroups.items():
        write_index_group(atomgroup, groupname=index_group_name, filename=f'{directory}{output_filename}',
                          write_option='a')


def read_gmx_dssp_count_file_to_pandas(file_path):
    """Reads a count.xvg file created by GROMACS do_dssp command into a pandas dataframe!"""

    import subprocess
    import re as re
    import pandas as pd

    directory, fname, ext = helper_find_dir_fname_ext(file_path)
    names = []
    lines_to_keep = []
    with open(f'{directory}{fname}{ext}', 'r') as file:
        for line in file.readlines():
            if line.startswith('#') or line.startswith('@'):
                if line.startswith('@'):
                    res = re.match('^@ s[0-9]* legend \".*"', line)
                    if res:
                        names.append(res.group(0).split()[-1][1:-1])
            else:
                lines_to_keep.append(line)
    with open(f'{directory}{fname}_processed{ext}', 'w') as file:
        for item in lines_to_keep:
            file.write(item)
    data = pd.read_csv(f'{directory}{fname}_processed{ext}',
                       names=names,
                       sep=r'\s+')
    subprocess.run(f'rm {directory}{fname}_processed{ext}', shell=True)
    return data


def read_gmx_dist_xyz_file_to_pandas(file_path):
    """Reads an xyz type file created by GROMACS distance, mindist etc. command into a pandas dataframe!"""

    import subprocess
    import pandas as pd

    directory, fname, ext = helper_find_dir_fname_ext(file_path)
    names = []
    lines_to_keep = []
    with open(f'{directory}{fname}{ext}', 'r') as file:
        for line in file.readlines():
            if not (line.startswith('#') or line.startswith('@')):
                lines_to_keep.append(line)
    number_of_columns = len(lines_to_keep[0].split()) - 1
    for index in range(1, int(number_of_columns / 3) + 1):
        names.append(f'X{str(index)}')
        names.append(f'Y{str(index)}')
        names.append(f'Z{str(index)}')
    with open(f'{directory}{fname}_processed{ext}', 'w') as file:
        for item in lines_to_keep:
            file.write(item)
    data = pd.read_csv(f'{directory}{fname}_processed{ext}',
                       names=names,
                       sep=r'\s+')
    subprocess.run(f'rm {directory}{fname}_processed{ext}', shell=True)
    return data


def read_gmx_dist_av_file_to_pandas(file_path):
    """Reads an avg type file created by GROMACS distance, mindist etc. command into a pandas dataframe!"""
    import subprocess
    import pandas as pd

    directory, fname, ext = helper_find_dir_fname_ext(file_path)
    names = []
    lines_to_keep = []
    with open(f'{directory}{fname}{ext}', 'r') as file:
        for line in file.readlines():
            if not (line.startswith('#') or line.startswith('@')):
                lines_to_keep.append(line)
    number_of_columns = len(lines_to_keep[0].split()) - 1
    for index in range(1, int(number_of_columns) + 1):
        names.append(f'av_dist{str(index)}')
        with open(f'{directory}{fname}_processed{ext}', 'w') as file:
            for item in lines_to_keep:
                file.write(item)
    data = pd.read_csv(f'{directory}{fname}_processed{ext}',
                       names=names,
                       sep=r'\s+')
    subprocess.run(f'rm {directory}{fname}_processed{ext}', shell=True)
    return data


def gmx_execute(command, **kwargs):
    """Executes a gmx command. Positional argument: the gmx command to exectue. Flags and parameters are keyword args.
    E.g.: f=traj.xtc,s=traj.tpr,v=True"""
    import subprocess

    cmd = f'gmx {str(command)} '
    for key, value in kwargs.items():
        if isinstance(value, bool):
            cmd += f'-{str(value)} '
        else:
            cmd += f'-{str(key)} {str(value)} '
    return subprocess.run(cmd, shell=True, capture_output=True)

from collections import namedtuple

_Level = namedtuple('Level', ['name', 'singular', 'plural'])

MOLECULELEVEL = _Level('molecule', Molecule, MoleculeGroup)

MoleculeGroup.level = MOLECULELEVEL
