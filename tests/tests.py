from gmxanal.misctools import Molecule,MoleculeGroup,molecule_parser,write_index_group,create_index_files
import MDAnalysis as mda
from copy import deepcopy
membranes = ['DOPC', 'DOPG', 'DOPS']
formal_membrane_names = {'DOPC':'PC', 'DOPG':'PC/PG', 'DOPS':'PC/PS'}
peptides = ['lasioglossin_III','FK_16','LL37','temporin_LA','macropin_1']
tp = ['dssp_count.xvg']
peptide_lengths = {'lasioglossin_III':15,'FK_16':16,'LL37':37,'temporin_LA':13,'macropin_1':13}
formal_peptide_names = {'lasioglossin_III':'Lasio III','FK_16':'FK-16','LL37':'LL-37','temporin_LA':'Tempo-La','macropin_1':'Macro1'}

root ='/home/kohut/gromacs/anticancer/membrane'
# for peptide_name in ['LL37']:
#     for membrane_name in ['DOPC']:
#         create_index_files(f'{root}/{peptide_name}/{membrane_name}/prod/md_{peptide_name}_{membrane_name}.xtc',
#                    f'{root}/{peptide_name}/{membrane_name}/prod/md_{peptide_name}_{membrane_name}.tpr',
#                    find_leaflet=True,
#                    bilayer_selection=f'resname {formal_membrane_names[membrane_name]}',
#                    output_filename='index_surface.ndx')

u = mda.Universe('/home/kohut/gromacs/CG/CM15_SUR/water/prod/md_surpep.tpr','/home/kohut/gromacs/CG/CM15_SUR/water/prod/md_mol_10ns.xtc')
sur_molecules = molecule_parser(u,'SUR','resname SUR')
cm15_molecules = molecule_parser(u,'CM15','protein')

for molecule in sur_molecules:
    print(molecule)
